
Duplicate env file ```.env-example``` to ```.env``` 

build:

	docker-compose build or docker-compose build app_name

up:

	docker-compose up -d or docker-compose up -d app_name

start:

	docker-compose start

down:

	docker-compose down

destroy:

	docker-compose down -v

stop:

	docker-compose stop

restart:

	docker-compose stop
	docker-compose up -d

logs:

	docker-compose logs --tail=100 -f

ps:

	docker-compose ps
